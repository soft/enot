#!/bin/sh -e
# Emergency notifications to ROSA users via SSH banner and GUI
# This script must be compatible with POSIX shell, use shellcheck to check it
# Author: Mikhail Novosyolov <m.novosyolov@rosalinux.ru>
# License: GPLv3 

get_session_type(){
	# https://unix.stackexchange.com/a/9607
	if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ] || [ -n "$SSH_CONNECTION" ]; then
		SESSION_TYPE='remote/ssh'
	else
		case "$(ps -o comm= -p $PPID)" in
			sshd|*/sshd) SESSION_TYPE='remote/ssh' ;;
		esac
	fi
	
	[ "$SESSION_TYPE" = 'remote/ssh' ] || return 1
}

get_exec_msg(){
	if [ -f '/usr/share/enot/enot-ssh-banner.sh' ]
		then exec_msg='/usr/share/enot/enot-ssh-banner.sh'
		elif [ -f './enot-ssh-banner.sh' ]; then exec_msg='./enot-ssh-banner.sh'
	fi
}

exec(){
	if [ -n "$exec_msg" ] && [ -x "$exec_msg" ]; then
		"$exec_msg"
	fi
}

get_session_type && \
get_exec_msg && \
exec

exit 0

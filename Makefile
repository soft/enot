NAME = enot
PREFIX = /
#SYSTEMD_UNIT_DIR = /lib/systemd
DATADIR = /usr/share
SYSCONFDIR = /etc

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/$(DATADIR)/$(NAME)/
	install -d $(DESTDIR)/$(PREFIX)/$(SYSCONFDIR)/profile.d/
	install -m0755 ./enot-ssh-banner.sh $(DESTDIR)/$(PREFIX)/$(DATADIR)/$(NAME)/
	install -m0755 ./enot-profile.sh $(DESTDIR)/$(PREFIX)/$(SYSCONFDIR)/profile.d/
	
uninstall:
	rm -fvr $(DESTDIR)/$(PREFIX)/$(DATADIR)/$(NAME)/*.sh
 

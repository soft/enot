#!/bin/sh -e
# Emergency notifications to ROSA users via SSH banner and GUI
# This script must be compatible with POSIX shell, use shellcheck to check it
# Author: Mikhail Novosyolov <m.novosyolov@rosalinux.ru>
# License: GPLv3 

lock_file="/var/local/enot-ssh-host-keys.lock"

echo_ssh_host_key_banner_ru(){
toilet -f bigmono9 'ROSA' || :
cat << EOF
>>> НОВОСТИ РОСЫ <<<

Вышла Rosa Fresh R11. Что нового:
* ...
* ...
* ...

Чтобы это сообщение больше не показывалось, выполните:
# touch $lock_file
EOF
}

echo_ssh_host_key_banner_en(){
toilet -f bigmono9 'ROSA' || :
cat << EOF
>>> ROSA NEWS <<<

Rosa Fresh R11 has been released. What's new:
* ...
* ...
* ...

To stop showing this message, execute:
# touch $lock_file
EOF
}

echo_ssh_host_key_banner(){
		if echo "$LANG" | grep -Ei "^ru_|^ru$" | grep -Eiq 'utf8|utf-8'
			then echo_ssh_host_key_banner_ru
			else echo_ssh_host_key_banner_en
		fi
}

[ ! -f "$lock_file" ] && echo_ssh_host_key_banner
